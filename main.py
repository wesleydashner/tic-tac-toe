from random import choice
from time import sleep

def print_board():
    if matrix[0][0] == 0:
        place1 = ' '
    if matrix[0][0] == 1:
        place1 = 'X'
    if matrix[0][0] == 2:
        place1 = 'O'
    if matrix[0][1] == 0:
        place2 = ' '
    if matrix[0][1] == 1:
        place2 = 'X'
    if matrix[0][1] == 2:
        place2 = 'O'
    if matrix[0][2] == 0:
        place3 = ' '
    if matrix[0][2] == 1:
        place3 = 'X'
    if matrix[0][2] == 2:
        place3 = 'O'
    if matrix[1][0] == 0:
        place4 = ' '
    if matrix[1][0] == 1:
        place4 = 'X'
    if matrix[1][0] == 2:
        place4 = 'O'
    if matrix[1][1] == 0:
        place5 = ' '
    if matrix[1][1] == 1:
        place5 = 'X'
    if matrix[1][1] == 2:
        place5 = 'O'
    if matrix[1][2] == 0:
        place6 = ' '
    if matrix[1][2] == 1:
        place6 = 'X'
    if matrix[1][2] == 2:
        place6 = 'O'
    if matrix[2][0] == 0:
        place7 = ' '
    if matrix[2][0] == 1:
        place7 = 'X'
    if matrix[2][0] == 2:
        place7 = 'O'
    if matrix[2][1] == 0:
        place8 = ' '
    if matrix[2][1] == 1:
        place8 = 'X'
    if matrix[2][1] == 2:
        place8 = 'O'
    if matrix[2][2] == 0:
        place9 = ' '
    if matrix[2][2] == 1:
        place9 = 'X'
    if matrix[2][2] == 2:
        place9 = 'O'

    print ' ---' * 3
    print '| ' + place1 + ' |' + ' ' + place2 + ' |' + ' ' + place3 + ' |'
    print ' ---' * 3
    print '| ' + place4 + ' |' + ' ' + place5 + ' |' + ' ' + place6 + ' |'
    print ' ---' * 3
    print '| ' + place7 + ' |' + ' ' + place8 + ' |' + ' ' + place9 + ' |'
    print ' ---' * 3

matrix = [[0, 0, 0],
          [0, 0, 0],
          [0, 0, 0]]

def test_win():
    for i in [1,2]:
        for index in range(3):
            if matrix[0][index] == i and matrix[1][index] == i and matrix[2][index] == i:
                if i == 1:
                    return "Player wins!"
                if i == 2:
                    return "AI wins!"
        for index in range(3):
            if matrix[index][0] == i and matrix[index][1] == i and matrix[index][2] == i:
                if i == 1:
                    return "Player wins!"
                if i == 2:
                    return "AI wins!"
        if matrix[0][0] == i and matrix[1][1] == i and matrix[2][2] == i:
            if i == 1:
                return "Player wins!"
            if i == 2:
                return "AI wins!"
        if matrix[0][2] == i and matrix[1][1] == i and matrix[2][0] == i:
            if i == 1:
                return "Player wins!"
            if i == 2:
                return "AI wins!"

counter = 0

def place_marker(player_input, marker):

    global matrix

    for i in range(1,4):
        if player_input[1] == i:
            matrix[i - 1][player_input[0] - 1] = marker

def test_self_win():

    global matrix

    for i in range(3):
        for index in range(3):
            if matrix[i][index] == 0:
                matrix[i][index] = 2
                if test_win() == "AI wins!":
                    return True
                else:
                    matrix[i][index] = 0

def test_opponent_win():

    global matrix

    for i in range(3):
        for index in range(3):
            if matrix[i][index] == 0:
                matrix[i][index] = 1
                if test_win() == "Player wins!":
                    matrix[i][index] = 2
                    return True
                else:
                    matrix[i][index] = 0

def test_corners():

    global matrix

    corner1 = matrix[0][0]
    corner2 = matrix[2][0]
    corner3 = matrix[0][2]
    corner4 = matrix[2][2]

    available_corners = []

    if corner1 == 0:
        available_corners.append('corner1')
    if corner2 == 0:
        available_corners.append('corner2')
    if corner3 == 0:
        available_corners.append('corner3')
    if corner4 == 0:
        available_corners.append('corner4')

    if available_corners:
        random_corner = choice(available_corners)
        if random_corner == 'corner1':
            matrix[0][0] = 2
            return True
        elif random_corner == 'corner2':
            matrix[2][0] = 2
            return True
        elif random_corner == 'corner3':
            matrix[0][2] = 2
            return True
        elif random_corner == 'corner4':
            matrix[2][2] = 2
            return True
        else:
            return False

def test_center():

    global matrix

    if matrix[1][1] == 0:
        matrix[1][1] = 2
        return True
    else:
        return False

def test_sides():

    global matrix

    side1 = matrix[0][1]
    side2 = matrix[0][1]
    side3 = matrix[2][1]
    side4 = matrix[1][2]

    available_sides = []

    if side1 == 0:
        available_sides.append('side1')
    if side2 == 0:
        available_sides.append('side2')
    if side3 == 0:
        available_sides.append('side3')
    if side4 == 0:
        available_sides.append('side4')

    random_side = choice(available_sides)

    if random_side == 'side1':
        matrix[0][0] = 2
        return True
    elif random_side == 'side2':
        matrix[2][0] = 2
        return True
    elif random_side == 'side3':
        matrix[0][2] = 2
        return True
    elif random_side == 'side4':
        matrix[2][2] = 2
        return True
    else:
        return False

print_board()

while True:

    if counter > 8:
        print "It's a tie!"
        break

    counter += 1

    while True:
        player_1_input = raw_input('Player 1: ')
        player_1_input = player_1_input.split(',')
        player_1_input[0] = int(player_1_input[0])
        player_1_input[1] = int(player_1_input[1])

        if matrix[player_1_input[1] - 1][player_1_input[0] - 1] == 0:
            place_marker(player_1_input, 1)
            break
        else:
            print "That space is not available!"

    print_board()

    if test_win():
        print test_win()
        break

    if counter > 8:
        print "It's a tie!"
        break

    counter += 1

    print "\nAI's turn",
    sleep(1)
    print ".",
    sleep(1)
    print ".",
    sleep(1)
    print".\n"

    if test_self_win():
        pass
    elif test_opponent_win():
        pass
    elif test_corners():
        pass
    elif test_center():
        pass
    elif test_sides():
        pass

    print_board()

    if test_win():
        print test_win()
        break
